# wine-build
A wine.git plugin script for xbuild.

## Prerequisites
1. [xbuild scripts](https://gitlab.freedesktop.org/2A4U/xbuild) - `git clone https://gitlab.freedesktop.org/2A4U/xbuild`
2. wine - `mkdir $HOME/xsrc` `cd xsrc` `git clone git://source.winehq.org/git/wine.git` Note: xsrc is the default directory for user variable $sourceDirectory.

## Getting Started
1. Within $HOME directory type: `git clone https://gitlab.freedesktop.org/2A4U/wine-build`
2. `linkplugin` - checks user variable $sourceDirectory for a match to plugin script. A symlink to matching script is created in $HOME/module_plugin.
3. `cdx` - an xbuild command.
4. `build wine`
